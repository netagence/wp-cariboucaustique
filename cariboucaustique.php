<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.netagence.com
 * @since             1.0.0
 * @package           Cariboucaustique
 *
 * @wordpress-plugin
 * Plugin Name:       Caribou Caustique
 * Plugin URI:        https://www.netagence.com
 * Description:       Displays random funny insults with a Quebec touch.
 * Version:           1.0.0
 * Author:            Pierre M.
 * Author URI:        https://www.netagence.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cariboucaustique
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CARIBOUCAUSTIQUE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cariboucaustique-activator.php
 */
function activate_cariboucaustique() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cariboucaustique-activator.php';
	Cariboucaustique_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cariboucaustique-deactivator.php
 */
function deactivate_cariboucaustique() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cariboucaustique-deactivator.php';
	Cariboucaustique_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cariboucaustique' );
register_deactivation_hook( __FILE__, 'deactivate_cariboucaustique' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cariboucaustique.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cariboucaustique() {

	$plugin = new Cariboucaustique();
	$plugin->run();

}
run_cariboucaustique();
