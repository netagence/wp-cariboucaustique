<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.netagence.com
 * @since      1.0.0
 *
 * @package    Cariboucaustique
 * @subpackage Cariboucaustique/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cariboucaustique
 * @subpackage Cariboucaustique/includes
 * @author     Pierre M. <ecrire@netagence.com>
 */
class Cariboucaustique_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
