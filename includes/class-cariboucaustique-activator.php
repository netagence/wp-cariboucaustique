<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.netagence.com
 * @since      1.0.0
 *
 * @package    Cariboucaustique
 * @subpackage Cariboucaustique/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cariboucaustique
 * @subpackage Cariboucaustique/includes
 * @author     Pierre M. <ecrire@netagence.com>
 */
class Cariboucaustique_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
