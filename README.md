# Caribou Caustique - WordPress Plugin

## Description

Caribou Caustique is a WordPress plugin that displays random quotes with a unique Québecois twist. It's perfect for adding a touch of humour and regional flavor to your WordPress site. Whether you're from Québec or just a fan of its culture, this plugin will surely bring a smile to your visitors' faces.

## Features

- Displays a random Québec-themed quote on your WordPress site.
- Easy to integrate via a shortcode in posts, pages, or widgets.
- Customizable display settings to match your site's design.

## Installation

1. **Download the Plugin**: Download the zip file from the WordPress plugin directory or from our GitHub repository.
   
2. **Upload to WordPress**: Go to your WordPress admin panel, navigate to 'Plugins > Add New', and choose 'Upload Plugin'. Select the downloaded zip file.

3. **Activate the Plugin**: After uploading the file, click on 'Install Now' and then activate the plugin.

4. **Configuration (Optional)**: Go to the Caribou Caustique settings page in your WordPress admin panel to customize the settings if needed.

## Usage

To display a random quote, simply use the shortcode `[caribou_caustique]` in your posts, pages, or text widgets. The quote will automatically appear where you place the shortcode.

## Customization

You can customize the appearance of the quotes via CSS. The plugin output is wrapped in a div with the class `.caribou-caustique-quote`, which you can target in your theme's stylesheet.

Example:
```
css
.quebec-quolibet-quote {
    font-style: italic;
    margin: 20px;
    /* Add more styles as per your requirement */
}
```


## Support

For support, please visit our support forum or GitHub issues page.

## Contributing

If you'd like to contribute to the development of Caribou Caustique, please visit our GitHub repository. We welcome all kinds of contributions, from bug reports to feature requests, and even code submissions.
License

Caribou Caustique is open-sourced software licensed under the GPLv2 license.
Acknowledgements

Special thanks to the vibrant culture of Québec for inspiring this project.
